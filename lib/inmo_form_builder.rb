class InmoFormBuilder < ActionView::Helpers::FormBuilder
    
    def form_error
        if self.object.errors.any?
            plural_name = self.object.class.model_name.plural
            model_name = self.object.class.model_name.human
            is_new = self.object.persisted? ? 'edit' : 'new'
            
            @template.content_tag :div, class: "form-error" do
                @template.content_tag :p, 'Ha ocurrido un error al guardar #{model_name}.'
            end
        end
    end
    
    def field_error(method)
        if self.object.errors[method].any? 
            @template.content_tag :span, self.object.errors[method].first ,class: "help-block", style: "color:red;"
        end
    end
end
