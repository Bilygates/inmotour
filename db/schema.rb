# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170503235354) do

  create_table "categories", force: :cascade do |t|
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "companions", force: :cascade do |t|
    t.string   "encrypted_nombre"
    t.string   "encrypted_seg_nombre"
    t.string   "encrypted_apellido"
    t.string   "encrypted_seg_apellido"
    t.string   "encrypted_documento"
    t.string   "pais_origen_doc"
    t.integer  "document_type_id"
    t.string   "sexo"
    t.integer  "nationality_id"
    t.date     "fecha_nac"
    t.string   "pais_residencia"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "encrypted_documento_iv"
    t.string   "encrypted_nombre_iv"
    t.string   "encrypted_apellido_iv"
    t.string   "encrypted_seg_apellido_iv"
    t.string   "encrypted_seg_nombre_iv"
    t.integer  "rental_id"
    t.index ["document_type_id"], name: "index_companions_on_document_type_id"
    t.index ["nationality_id"], name: "index_companions_on_nationality_id"
    t.index ["rental_id"], name: "index_companions_on_rental_id"
  end

  create_table "cottages", force: :cascade do |t|
    t.string   "nombre"
    t.integer  "capacidad"
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "tipo"
    t.integer  "person_id"
    t.index ["person_id"], name: "index_cottages_on_person_id"
  end

  create_table "document_types", force: :cascade do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emails", force: :cascade do |t|
    t.string   "encrypted_descripcion"
    t.string   "encrypted_email"
    t.integer  "person_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "encrypted_descripcion_iv"
    t.string   "encrypted_email_iv"
    t.index ["person_id"], name: "index_emails_on_person_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.integer  "importe"
    t.string   "descripcion"
    t.date     "fecha"
    t.integer  "category_id"
    t.integer  "person_id"
    t.integer  "cottage_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["category_id"], name: "index_expenses_on_category_id"
    t.index ["cottage_id"], name: "index_expenses_on_cottage_id"
    t.index ["person_id"], name: "index_expenses_on_person_id"
  end

  create_table "identities", force: :cascade do |t|
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_digest"
    t.string   "admin"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "incomes", force: :cascade do |t|
    t.date     "fecha"
    t.string   "tipo"
    t.integer  "importe"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "rental_id"
    t.index ["rental_id"], name: "index_incomes_on_rental_id"
  end

  create_table "nationalities", force: :cascade do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "codigo"
  end

  create_table "people", force: :cascade do |t|
    t.string   "encrypted_documento"
    t.string   "encrypted_nombres"
    t.string   "encrypted_apellidos"
    t.string   "encrypted_ciudad"
    t.string   "encrypted_domicilio"
    t.date     "fecha_nac"
    t.string   "encrypted_matricula_coche"
    t.string   "tipo"
    t.integer  "nationality_id"
    t.integer  "document_type_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "encrypted_segundo_ape"
    t.string   "encrypted_segundo_nom"
    t.string   "pais_residencia"
    t.string   "pais_origen_doc"
    t.string   "sexo"
    t.string   "encrypted_cuenta_banco"
    t.string   "encrypted_cuenta_banco_iv"
    t.string   "encrypted_documento_iv"
    t.string   "encrypted_nombres_iv"
    t.string   "encrypted_apellidos_iv"
    t.string   "encrypted_ciudad_iv"
    t.string   "encrypted_domicilio_iv"
    t.string   "encrypted_matricula_coche_iv"
    t.string   "encrypted_segundo_ape_iv"
    t.string   "encrypted_segundo_nom_iv"
    t.index ["document_type_id"], name: "index_people_on_document_type_id"
    t.index ["nationality_id"], name: "index_people_on_nationality_id"
  end

  create_table "phones", force: :cascade do |t|
    t.string   "encrypted_descripcion"
    t.integer  "person_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "encrypted_descripcion_iv"
    t.string   "encrypted_numero_iv"
    t.string   "encrypted_numero"
    t.index ["person_id"], name: "index_phones_on_person_id"
  end

  create_table "providers", force: :cascade do |t|
    t.string   "encrypted_nombre"
    t.string   "encrypted_web"
    t.string   "encrypted_rut"
    t.string   "encrypted_domicilio"
    t.string   "encrypted_telefono"
    t.string   "encrypted_celular"
    t.string   "encrypted_email"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "encrypted_nombre_iv"
    t.string   "encrypted_web_iv"
    t.string   "encrypted_rut_iv"
    t.string   "encrypted_domicilio_iv"
    t.string   "encrypted_telefono_iv"
    t.string   "encrypted_celular_iv"
    t.string   "encrypted_email_iv"
  end

  create_table "rentals", force: :cascade do |t|
    t.date     "fechadesde"
    t.date     "fechahasta"
    t.integer  "valordia"
    t.integer  "medidoringreso"
    t.integer  "medidorfin"
    t.integer  "valorkw"
    t.integer  "cant_acomp"
    t.string   "descripcion"
    t.integer  "person_id"
    t.integer  "cottage_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.date     "fecha_entrega"
    t.time     "hora_entrega"
    t.integer  "costo_total"
    t.string   "estado"
    t.integer  "ganancia_dueno"
    t.string   "transporte"
    t.string   "informado"
    t.time     "hora_entrada"
    t.integer  "total_luz"
    t.string   "pago_luz"
    t.index ["cottage_id"], name: "index_rentals_on_cottage_id"
    t.index ["person_id"], name: "index_rentals_on_person_id"
  end

end
