class AddRentalIdToCompanion < ActiveRecord::Migration[5.0]
  def change
      add_reference :companions, :rental, index: true
  end
end
