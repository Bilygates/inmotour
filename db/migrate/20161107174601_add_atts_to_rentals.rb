class AddAttsToRentals < ActiveRecord::Migration[5.0]
  def change
    add_column :rentals, :fecha_entrega, :date
    add_column :rentals, :hora_entrega, :time
    add_column :rentals, :costo_total, :integer
    add_column :rentals, :estado, :string
    add_column :rentals, :ganancia_dueno, :integer
    add_column :rentals, :transporte, :string
  end
end
