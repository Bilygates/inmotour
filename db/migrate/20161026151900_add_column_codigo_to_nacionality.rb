class AddColumnCodigoToNacionality < ActiveRecord::Migration[5.0]
  def change
    add_column :nationalities, :codigo, :string
  end
end
