class AddEncriptPeople < ActiveRecord::Migration[5.0]
  def change
    rename_column :people, :documento, :encrypted_documento
    add_column :people, :encrypted_documento_iv, :string
    
    rename_column :people, :nombres, :encrypted_nombres
    add_column :people, :encrypted_nombres_iv, :string
    
    rename_column :people, :apellidos, :encrypted_apellidos
    add_column :people, :encrypted_apellidos_iv, :string
    
    rename_column :people, :ciudad, :encrypted_ciudad
    add_column :people, :encrypted_ciudad_iv, :string
    
    rename_column :people, :domicilio, :encrypted_domicilio
    add_column :people, :encrypted_domicilio_iv, :string
    
    rename_column :people, :matricula_coche, :encrypted_matricula_coche
    add_column :people, :encrypted_matricula_coche_iv, :string
    
    rename_column :people, :segundo_ape, :encrypted_segundo_ape
    add_column :people, :encrypted_segundo_ape_iv, :string
    
    rename_column :people, :segundo_nom, :encrypted_segundo_nom
    add_column :people, :encrypted_segundo_nom_iv, :string
  end
end
