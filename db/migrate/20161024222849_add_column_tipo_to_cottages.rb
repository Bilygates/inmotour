class AddColumnTipoToCottages < ActiveRecord::Migration[5.0]
  def change
    add_column :cottages, :tipo, :string
  end
end
