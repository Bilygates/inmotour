class CreateRentals < ActiveRecord::Migration[5.0]
  def change
    create_table :rentals do |t|
      t.date :fechadesde
      t.date :fechahasta
      t.integer :valordia
      t.integer :medidoringreso
      t.integer :medidorfin
      t.integer :valorkw
      t.integer :cant_acomp
      t.string :descripcion
      t.references :person, foreign_key: true
      t.references :cottage, foreign_key: true

      t.timestamps
    end
  end
end
