class AddEncriptMails < ActiveRecord::Migration[5.0]
  def change
    rename_column :emails, :descripcion, :encrypted_descripcion
    add_column :emails, :encrypted_descripcion_iv, :string
    
    rename_column :emails, :email, :encrypted_email
    add_column :emails, :encrypted_email_iv, :string
  end
end
