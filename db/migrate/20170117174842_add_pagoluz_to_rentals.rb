class AddPagoluzToRentals < ActiveRecord::Migration[5.0]
  def change
    add_column :rentals, :total_luz, :integer
    add_column :rentals, :pago_luz, :string
  end
end
