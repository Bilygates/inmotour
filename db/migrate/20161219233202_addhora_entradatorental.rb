class AddhoraEntradatorental < ActiveRecord::Migration[5.0]
  def change
    add_column :rentals, :hora_entrada, :time
  end
end
