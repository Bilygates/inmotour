class AddPhonesString < ActiveRecord::Migration[5.0]
  def change
    remove_column :phones, :encrypted_numero
    add_column :phones, :encrypted_numero, :string
  end
end
