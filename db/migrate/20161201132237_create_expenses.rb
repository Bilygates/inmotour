class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.integer :importe
      t.string :descripcion
      t.date :fecha
      t.references :category, foreign_key: true
      t.references :people, foreign_key: true
      t.references :cottage, foreign_key: true
      t.timestamps
    end
  end
end
