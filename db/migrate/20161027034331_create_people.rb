class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :documento
      t.string :nombres
      t.string :apellidos
      t.string :ciudad
      t.string :domicilio
      t.date :fecha_nac
      t.string :matricula_coche
      t.string :tipo
      t.belongs_to :nationality, foreign_key: true
      t.belongs_to :document_type, foreign_key: true

      t.timestamps
    end
  end
end
