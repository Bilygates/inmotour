class CreateIncomes < ActiveRecord::Migration[5.0]
  def change
    create_table :incomes do |t|
      t.date :fecha
      t.string :tipo
      t.integer :importe

      t.timestamps
    end
  end
end
