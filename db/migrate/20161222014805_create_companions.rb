class CreateCompanions < ActiveRecord::Migration[5.0]
  def change
    create_table :companions do |t|
      t.string :nombre
      t.string :seg_nombre
      t.string :apellido
      t.string :seg_apellido
      t.string :documento
      t.string :pais_origen_doc
      t.references :document_type, foreign_key: true
      t.string :sexo
      t.references :nationality, foreign_key: true
      t.date :fecha_nac
      t.string :pais_residencia

      t.timestamps
    end
  end
end
