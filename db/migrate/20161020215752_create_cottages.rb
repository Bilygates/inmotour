class CreateCottages < ActiveRecord::Migration[5.0]
  def change
    create_table :cottages do |t|
      t.string :nombre
      t.integer :capacidad
      t.string :descripcion

      t.timestamps
    end
  end
end
