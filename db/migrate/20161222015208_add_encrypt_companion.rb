class AddEncryptCompanion < ActiveRecord::Migration[5.0]
  def change
    rename_column :companions, :documento, :encrypted_documento
    add_column :companions, :encrypted_documento_iv, :string
    
    rename_column :companions, :nombre, :encrypted_nombre
    add_column :companions, :encrypted_nombre_iv, :string
    
    rename_column :companions, :apellido, :encrypted_apellido
    add_column :companions, :encrypted_apellido_iv, :string
    
    rename_column :companions, :seg_apellido, :encrypted_seg_apellido
    add_column :companions, :encrypted_seg_apellido_iv, :string
    
    rename_column :companions, :seg_nombre, :encrypted_seg_nombre
    add_column :companions, :encrypted_seg_nombre_iv, :string
  end
end
