class Encriptarcuenta < ActiveRecord::Migration[5.0]
  def change
    remove_column :people, :encrypted_cuenta
   
    add_column :people, :encrypted_cuenta_banco, :string
  end
end
