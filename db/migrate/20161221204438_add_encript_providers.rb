class AddEncriptProviders < ActiveRecord::Migration[5.0]
  def change
    rename_column :providers, :nombre, :encrypted_nombre
    add_column :providers, :encrypted_nombre_iv, :string
    
    rename_column :providers, :web, :encrypted_web
    add_column :providers, :encrypted_web_iv, :string
    
    rename_column :providers, :rut, :encrypted_rut
    add_column :providers, :encrypted_rut_iv, :string
    
    rename_column :providers, :domicilio, :encrypted_domicilio
    add_column :providers, :encrypted_domicilio_iv, :string
    
    rename_column :providers, :telefono, :encrypted_telefono
    add_column :providers, :encrypted_telefono_iv, :string
    
    rename_column :providers, :celular, :encrypted_celular
    add_column :providers, :encrypted_celular_iv, :string
    
    rename_column :providers, :email, :encrypted_email
    add_column :providers, :encrypted_email_iv, :string
  end
end
