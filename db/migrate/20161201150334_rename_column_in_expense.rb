class RenameColumnInExpense < ActiveRecord::Migration[5.0]
  def change
     rename_column :expenses, :people_id, :person_id
  end
end
