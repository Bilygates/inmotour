class AddPersonIdToCottage < ActiveRecord::Migration[5.0]
  def change
     add_reference :cottages, :person, index: true
  end
end
