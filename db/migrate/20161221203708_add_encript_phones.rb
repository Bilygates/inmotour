class AddEncriptPhones < ActiveRecord::Migration[5.0]
  def change
    rename_column :phones, :descripcion, :encrypted_descripcion
    add_column :phones, :encrypted_descripcion_iv, :string
    
    rename_column :phones, :numero, :encrypted_numero
    add_column :phones, :encrypted_numero_iv, :string
  end
end
