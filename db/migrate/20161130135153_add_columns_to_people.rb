class AddColumnsToPeople < ActiveRecord::Migration[5.0]
  def change
    add_column :people, :segundo_ape, :string
    add_column :people, :segundo_nom, :string
    add_column :people, :pais_residencia, :string
    add_column :people, :pais_origen_doc, :string
    add_column :people, :sexo, :string
  end
end
