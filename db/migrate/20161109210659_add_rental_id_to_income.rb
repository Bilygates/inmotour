class AddRentalIdToIncome < ActiveRecord::Migration[5.0]
  def change
    add_reference :incomes, :rental, index: true
  end
end
