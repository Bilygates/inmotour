# :nodoc:
module ApplicationHelper
  def left_menu
    left_menu_entries(left_menu_content)
  end

  private

  def selected_locale
    locale = FastGettext.locale
    locale_list.detect {|entry| entry[:locale] == locale}
  end

  def locale_list
    [
      {
        flag: 'us',
        locale: 'en',
        name: 'English (US)',
        alt_name: 'United States'
      },
      {
        flag: 'fr',
        locale: 'fr',
        name: 'Français',
        alt_name: 'France'
      },
      {
        flag: 'es',
        locale: 'es',
        name: 'Español',
        alt_name: 'Spanish'
      },
      {
        flag: 'de',
        locale: 'de',
        name: 'Deutsch',
        alt_name: 'German'
      },
      {
        flag: 'jp',
        locale: 'ja',
        name: '日本語',
        alt_name: 'Japan'
      },
      {
        flag: 'cn',
        locale: 'zh',
        name: '中文',
        alt_name: 'China'
      },
      {
        flag: 'it',
        locale: 'it',
        name: 'Italiano',
        alt_name: 'Italy'
      },
      {
        flag: 'pt',
        locale: 'pt',
        name: 'Portugal',
        alt_name: 'Portugal'
      },
      {
        flag: 'ru',
        locale: 'ru',
        name: 'Русский язык',
        alt_name: 'Russia'
      },
      {
        flag: 'kr',
        locale: 'kr',
        name: '한국어',
        alt_name: 'Korea'
      },
    ]
  end

  def left_menu_entries(entries = [])
    output = ''
    entries.each do |entry|      
        children_selected = entry[:children] &&
          entry[:children].any? {|child| current_page?(child[:href]) }
        entry_selected =  current_page?(entry[:href])
        li_class =
        case
          when children_selected
            'open'
          when entry_selected
            'active'
        end
        output +=
          content_tag(:li, class: li_class) do
            subentry = ''
            subentry +=
              link_to entry[:href], title: entry[:title], class: entry[:class], target: entry[:target] do
                link_text = ''
                link_text += entry[:content].html_safe
                if entry[:children]
                  if children_selected
                    link_text += '<b class="collapse-sign"><em class="fa fa-minus-square-o"></em></b>'
                  else
                    link_text += '<b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>'
                  end
                end
  
                link_text.html_safe
              end
            if entry[:children]
              if children_selected
                ul_style = 'display: block'
              else
                ul_style = ''
              end
              subentry +=
                "<ul style='#{ul_style}'>" +
                  left_menu_entries(entry[:children]) +
                  "</ul>"
            end
  
            subentry.html_safe
          end      
    end
    output.html_safe
  end

  def left_menu_content
    left_menu_components =    
    [
      {
        href: root_path,
        title: _('Inicio'),
        content: "<i class='fa fa-lg fa-fw fa-home'></i> <span class='menu-item-parent'>" + _('Inicio') + "</span>",
      },
      {
        href: rentals_path,
        title: _('Alquileres'),
        content: "<i class='fa fa-lg fa-fw fa-calendar-o'></i> <span class='menu-item-parent'>" + _('Alquileres') + "</span>",
      },
      
      {
        href: cottages_path,
        title: _('Casas'),
        content: "<i class='fa fa-lg fa-fw fa-home'></i> <span class='menu-item-parent'>" + _('Casas') + "</span>",
      },
      
      {
        href: incomes_path,
        title: _('Ingresos'),
        content: "<i class='fa fa-lg fa-fw fa-line-chart'></i> <span class='menu-item-parent'>" + _('Ingresos') + "</span>",
      },
      {
        href: expenses_path,
        title: _('Gastos'),
        content: "<i class='fa fa-lg fa-fw fa-bar-chart'></i> <span class='menu-item-parent'>" + _('Gastos') + "</span>",
      },
      {
        href: calendariocasa_path ,
        title: _('Disponibilidad Casas'),
        content: "<i class='fa fa-lg fa-fw fa-calendar'></i> <span class='menu-item-parent'>" + _('Disponibilidad Casas') + "</span>",
          
      },
     
      
       {
          href: '#',
          content: "<i class='fa fa-lg fa-fw fa-group'></i> <span class='menu-item-parent'>" + _('Personas') + "</span>",
          children: [
              {
                href: clientes_path,
                title: _('Clientes'),
                content: "<i class='fa fa-lg fa-fw fa-group'></i> <span class='menu-item-parent'>" + _('Clientes') + "</span>",
              },
              {
                href: people_path,
                title: _('Dueños'),
                content: "<i class='fa fa-lg fa-fw fa-group'></i> <span class='menu-item-parent'>" + _('Dueños') + "</span>",
              },
              {
                href: companions_path,
                title: _('Acompañantes'),
                content: "<i class='fa fa-lg fa-fw fa-group'></i> <span class='menu-item-parent'>" + _('Acompañantes') + "</span>",
              },
            ] 
        },
      
          {
            href: '#',
            content: "<i class='fa fa-lg fa-fw fa-clipboard'></i> <span class='menu-item-parent'>" + _('Reportes') + "</span>",
            children: [
              
                {
                  href: '#',
                  content: "<i class='fa fa-lg fa-fw fa-download'></i> <span class='menu-item-parent'>" + _('Reporte Interpol') + "</span>",
                  children: [
                        {
                          href: clientes_rentas_path,
                          title: _('Registro Informático de Huéspedes y Pasajeros (RIHP)'),
                          content: "<i class='fa fa-lg fa-fw fa-cube'></i> <span class='menu-item-parent'>" + _('Reporte RIHP') + "</span>",
                        },
                        {
                          href: clientes_rentas_faltantes_path,
                          title: _('Personas con datos incompletos'),
                          content: "<i class='fa fa-lg fa-fw fa-exclamation-circle'></i> <span class='menu-item-parent'>" + _('Datos Faltantes') + "</span>",
                        },
                      ]
                },
            
                  {
                    href: balance_path ,
                    title: _('Reporte Balance'),
                    content: "<i class='fa fa-lg fa-fw fa-line-chart'></i> <span class='menu-item-parent'>" + _('Balance') + "</span>",
                  },
                  
                  {
                    href: reporte_ganancias_path ,
                    title: _('Reporte Ganancias'),
                    content: "<i class='fa fa-lg fa-fw fa-line-chart'></i> <span class='menu-item-parent'>" + _('Ganancias Dueños') + "</span>",
                  },
                
                  
                ]
          },
          
          
        
        {
          href: '#',
          content: "<i class='fa fa-lg fa-fw fa-list-alt'></i> <span class='menu-item-parent'>" + _('Agendas') + "</span>",
          children: [
                {
                  href: phones_path,
                  title: _('Agenda Telefónica'),
                  content: "<i class='fa fa-lg fa-fw fa-phone'></i> <span class='menu-item-parent'>" + _('Agenda Telefónica') + "</span>",
                },
                {
                  href: emails_path,
                  title: _('Agenda emails'),
                  content: "<i class='fa fa-lg fa-fw fa-envelope-o'></i> <span class='menu-item-parent'>" + _('Agenda emails') + "</span>",
                },
                {
                  href: agendacuentas_path,
                  title: _('Agenda Cuentas Bancarias'),
                  content: "<i class='fa fa-lg fa-fw fa-bank'></i> <span class='menu-item-parent'>" + _('Agenda Cuentas') + "</span>",
                },
          ] },
      
      
      href: '#',
      content: "<i class='fa fa-lg fa-fw fa-puzzle-piece'></i> <span class='menu-item-parent'>" + _('Configuración Sistema') + "</span>",
      children: [
        {
          href: categories_path,
          title: _('Categorías Gastos'),
          content: "<i class='fa fa-lg fa-fw fa-gears'></i> <span class='menu-item-parent'>" + _('Categorías Gastos') + "</span>",
        },
        {
          href: nationalities_path,
          title: _('Nacionalidades'),
          content: "<i class='fa fa-lg fa-fw fa-flag'></i> <span class='menu-item-parent'>" + _('Nacionalidades') + "</span>",
        },
                {
          href: document_types_path,
          title: _('Tipos de Documentos'),
          content: "<i class='fa fa-lg fa-fw fa-indent'></i> <span class='menu-item-parent'>" + _('Tipos de Documentos') + "</span>",
        },
        {
          href: identities_path,
          title: _('Usuarios'),
          content: "<i class='fa fa-lg fa-fw fa-user'></i> <span class='menu-item-parent'>" + _('Usuarios') + "</span>",
        },
        {
          href: providers_path,
          title: _('Proveedores'),
          content: "<i class='fa fa-lg fa-fw fa-truck'></i> <span class='menu-item-parent'>" + _('Proveedores') + "</span>",
        },


      ]    
      
    ]
    
 
  end

end
