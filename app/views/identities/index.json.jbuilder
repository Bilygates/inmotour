json.array!(@identities) do |identity|
  json.extract! identity, :id, :email, :password_hash, :password_digest, :admin, :photo
  json.url identity_url(identity, format: :json)
end
