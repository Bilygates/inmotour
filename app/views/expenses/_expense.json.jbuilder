json.extract! expense, :id, :importe, :descripcion, :fecha, :category_id, :person_id, :cottage_id, :created_at, :updated_at
json.url expense_url(expense, format: :json)