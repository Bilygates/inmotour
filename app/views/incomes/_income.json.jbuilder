json.extract! income, :id, :fecha, :tipo, :importe, :created_at, :updated_at
json.url income_url(income, format: :json)