json.extract! provider, :id, :nombre, :web, :rut, :domicilio, :telefono, :celular, :email, :created_at, :updated_at
json.url provider_url(provider, format: :json)