class Category < ApplicationRecord
    has_many :expenses
    validates :descripcion, presence: true
end
