class Provider < ApplicationRecord
    attr_encrypted_options.merge!(:encode => true)
    attr_encrypted :nombre, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :web, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :rut, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :domicilio, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :telefono, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :celular, :key => 'asdasd3434341231235454578...----1113123'
    attr_encrypted :email, :key => 'asdasd3434341231235454578...----1113123'
end
