class Email < ApplicationRecord
  attr_encrypted_options.merge!(:encode => true)
  attr_encrypted :descripcion, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :email, :key => 'asdasd3434341231235454578...----1113123'

  belongs_to :person, required: false
end
