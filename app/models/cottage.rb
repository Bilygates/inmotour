class Cottage < ApplicationRecord
    belongs_to :person, required: false
    has_many :rentals, dependent: :destroy
    has_many :expenses, dependent: :destroy
    
end
