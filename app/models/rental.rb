class Rental < ApplicationRecord
  include AASM

  belongs_to :person , required: false
  belongs_to :cottage, required: false
  has_many :incomes, dependent: :destroy
  has_many :companions, dependent: :destroy

  
  accepts_nested_attributes_for :companions, reject_if: proc { |attributes| attributes['nombre'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :incomes, reject_if: proc { |attributes| attributes['importe'].blank? }, allow_destroy: true

  validate :fecha_libre
  validate :ganancia

  
  def fecha_libre
      @rentals = Rental.where.not(:id => id).where(cottage_id: cottage_id).where(fechadesde: fechadesde..fechahasta).where.not(estado: 'cancelada')
      @ok = 'S'
      if @rentals.any?
        @rentals.each do |r|
          if r.fecha_entrega > r.fechahasta
            @ok = 'N'
          end
        end
        if @ok == 'N'
          errors.messages[:fechadesde] << "La fecha ya se encuentra ocupada."
          errors.messages[:fechahasta] << "La fecha ya se encuentra ocupada."
        end  
      end
      if @ok == 'S'
        @rentals = Rental.where.not(:id => id).where(cottage_id: cottage_id).where(fechahasta: fechadesde..fechahasta).where.not(fecha_entrega: fechadesde..fechahasta).where.not(estado: 'cancelada')
        if @rentals.any?
          errors.messages[:fechadesde] << "La fecha ya se encuentra ocupada."
          errors.messages[:fechahasta] << "La fecha ya se encuentra ocupada."
        else  
          @rentals = Rental.where.not(:id => id).where(cottage_id: cottage_id).where(fecha_entrega: fechadesde).where("hora_entrega > ?", hora_entrada).where.not(estado: 'cancelada')   
          if @rentals.any?
            errors.messages[:hora_entrada] << "La casa todavía esta ocupada a esa hora."
          end
        end
      
      end
    
  end
  
  def ganancia
    if ganancia_dueno
      if ganancia_dueno > 0
        if ganancia_dueno > costo_total
          errors.messages[:ganancia_dueno] << "La ganancia no puede ser mayor al total."
        end
      else  
        return true
      end
    else
      return true
    end
  end
  
  def casa_cliente
    "#{self.cottage.nombre}, #{self.person.nombres} #{self.person.apellidos}, Total: $  #{self.importe_total}, Remanente: $  #{self.importe_restante} "
  end
  
  def importe_restante
    @suma =  Income.where(:rental_id => id).sum(:importe)
    @importe_total_ax = importe_total
    if @importe_total_ax
      @restante = @importe_total_ax - @suma 
    else
        @restante = 0
    end
    return @restante
  end
  
  def importe_total
    if total_luz
      @importe_total =  costo_total + total_luz
    else
      @importe_total =  costo_total
    end
    
    return @importe_total
  end
  
  
  aasm column: "estado" do
    state :ingresada, initial: true
    state :confirmada
    state :encurso
    state :finalizada
    state :cancelada
    
    event :confirmar do 
       transitions from: :ingresada, to: :confirmada
    end
    event :desconfirmar do 
       transitions from: :confirmada, to: :ingresada
    end
    event :cursar do 
       transitions from: :confirmada, to: :encurso
    end
    event :cancelar do 
       transitions from: :ingresada, to: :cancelada
       transitions from: :confirmada, to: :cancelada
       transitions from: :encurso, to: :cancelada
    end
    event :finalizar do 
      transitions from: :encurso, to: :finalizada
      transitions from: :confirmada, to: :finalizada
    end
    
  end
  
end
