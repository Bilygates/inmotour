class Companion < ApplicationRecord
  attr_encrypted_options.merge!(:encode => true)

  attr_encrypted :documento, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :nombre, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :apellido, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :seg_apellido, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :seg_nombre, :key => 'asdasd3434341231235454578...----1113123'
  
  
  belongs_to :document_type
  belongs_to :nationality
  belongs_to :rental
end
