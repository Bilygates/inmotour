class DocumentType < ApplicationRecord
    has_many :people
    has_many :companions
end
