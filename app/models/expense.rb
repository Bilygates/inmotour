class Expense < ApplicationRecord
  belongs_to :category, required: false
  belongs_to :person , required: false
  belongs_to :cottage
  belongs_to :rental, required: false
end
