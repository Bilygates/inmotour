class Person < ApplicationRecord
  attr_encrypted_options.merge!(:encode => true)
  attr_encrypted :cuenta_banco, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :documento, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :nombres, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :apellidos, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :ciudad, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :domicilio, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :matricula_coche, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :segundo_ape, :key => 'asdasd3434341231235454578...----1113123'
  attr_encrypted :segundo_nom, :key => 'asdasd3434341231235454578...----1113123'


  belongs_to :nationality, required: false
  belongs_to :document_type, required: false
  has_many :emails, dependent: :destroy
  has_many :phones, dependent: :destroy
  has_many :rentals, dependent: :destroy
  has_many :expenses, dependent: :destroy
  
  accepts_nested_attributes_for :phones, reject_if: proc { |attributes| attributes['numero'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :emails, reject_if: proc { |attributes| attributes['email'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :rentals,  allow_destroy: true
  
  
  def nom_ape
      "#{nombres}, #{apellidos}"
  end
  
  
end
