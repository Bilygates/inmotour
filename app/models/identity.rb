class Identity < ActiveRecord::Base
  has_secure_password validations: true

  validates :email, presence: true, uniqueness: true
  validates :password_confirmation, presence: true, if: -> r { r.password.present? } 
  has_attached_file :avatar, styles: { medium: '200x200>', thumb: '48x48>' }, :default_url => "avatars/male_thumb.png" 
  #validates_attachment_presence :avatar
  validates_attachment :avatar, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
end
