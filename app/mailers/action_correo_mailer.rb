class ActionCorreoMailer < ApplicationMailer
  default from: '@gmail.com'
  layout 'mailer'
  
  def bienvenido_email(email)
    @email = email
    @url  = 'http://codeHero.co'
    mail(to:  @email, subject: 'prueba')
  end
end
