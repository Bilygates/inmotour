# :nodoc:
class ApplicationController < ActionController::Base
  before_filter :set_gettext_locale
  protect_from_forgery with: :exception

  helper_method :current_identity, :identity_signed_in?, :warden_message, :is_admin?
  
  
protected
  def current_identity
    warden.user(scope: :identity)
  end

  def identity_signed_in?
    warden.authenticate?(scope: :identity)
  end

  def authenticate!
    redirect_to root_path, alert: 'Credenciales erróneas' unless identity_signed_in?
  end

  def warden_message
    warden.message
  end

  def warden
    request.env['warden']
  end
  
  def is_admin?
    if current_identity.admin == 1
      true
    end
  end
  
end
  
  
  

