class CottagesController < ApplicationController
  before_action :set_cottage, only: [:show, :edit, :update, :destroy ]
  before_action :authenticate!
  # GET /cottages/
  # GET /cottages.json
  def reporte
    @rentals = Rental.where(:cottage_id => params[:id_rep])
  end
  
  def calendariocasa
    @cottages = Cottage.all
    @rentals = Rental.all
  end
  def index
    @cottages = Cottage.all
  end

  # GET /cottages/1
  # GET /cottages/1.json
  def show
  end

  # GET /cottages/new
  def new
    @cottage = Cottage.new
  end

  # GET /cottages/1/edit
  def edit
  end

  # POST /cottages
  # POST /cottages.json
  def create
    @cottage = Cottage.new(cottage_params)
    
    respond_to do |format|
      if @cottage.save
        format.html { redirect_to cottages_path , notice: 'Casa guardada correctamente.' }
      else
        format.html { render :new }
        format.json { render json: @cottage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cottages/1
  # PATCH/PUT /cottages/1.json
  def update
    
    respond_to do |format|
      if @cottage.update(cottage_params)
        format.html { redirect_to cottages_path, notice: 'Casa actualizada correctamente.' }
      else
        format.html { render :edit }
        format.json { render json: @cottage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cottages/1
  # DELETE /cottages/1.json
  def destroy
    @cottage.destroy
    
    respond_to do |format|
      format.html { redirect_to cottages_url, notice: 'Casa eliminada correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cottage
      @cottage = Cottage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cottage_params
      params.require(:cottage).permit(:nombre, :capacidad, :descripcion,:tipo, :person_id, :rep_id, )
    end
end
