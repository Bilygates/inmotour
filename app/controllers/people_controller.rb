class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy]
  before_action :authenticate!
  # GET /people
  # GET /people.json
  def index
    @people = Person.where(:tipo => "D")
  end
  
  def index_cliente
    @people = Person.where(:tipo => "C")
  end

  
  def clientes_rentas
    @rentals = Rental.all
    @companions = Companion.all
    @continuar = "S"
  end
  
  def clientes_rentas_faltantes
    @rentals = Rental.all
    @companions = Companion.all
  end

  def duenoscobro
    #User.joins(:posts).where("posts.created_at < ?", Time.now)
    @rentals = Rental.joins("INNER JOIN cottages ON cottages.id = rentals.cottage_id AND cottages.person_id ="+params[:id_dueno])
    @dueno = Person.find(params[:id_dueno])
  end
 
 
  def reporte_ganancias
     @duenos = Person.where(:tipo => "D")  
  end
  
  def reporte_ganancias_detalle
   
    @id = params[:id_dueno][:person_id]
    
    @dueno = Person.find(@id)
    @fecha_desde = params[:fecha_desde].to_datetime.strftime("%Y-%m-%d")
    @fecha_hasta = params[:fecha_hasta].to_datetime.strftime("%Y-%m-%d")
    
    @rentals = Rental.find_by_sql("SELECT rentals.* FROM rentals, cottages WHERE cottages.id = rentals.cottage_id AND cottages.person_id =" + @id + " AND cottages.tipo = 'D' AND rentals.fechadesde >= '" + @fecha_desde +"' and rentals.fechadesde <= '" + @fecha_hasta + "' AND rentals.estado <> 'cancelada' order by rentals.fechadesde  ")

    @gastos = Expense.find_by_sql("SELECT e.* FROM expenses e, cottages c WHERE e.fecha >= '" +  @fecha_desde + "' AND e.fecha <= '" + @fecha_hasta + "' AND e.cottage_id  = c.id AND c.person_id = " + @id + " AND c.tipo= 'D' order by e.fecha" )
  end
 
  def agendacuentas
    @people = Person.where(:tipo => "D")
  end
  
  # GET /people/1
  # GET /people/1.json
  def show
    @person.phones.build 
    @person.emails.build
  end

  # GET /people/new
  def new
    @person = Person.new
    @person.phones.build 
    @person.emails.build
    @person.tipo = params[:tipo]
  end

  # GET /people/1/edit
  def edit
    @person.phones.build 
    @person.emails.build
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)
      
    respond_to do |format|
      if @person.save
        @person.rentals.each do |r|
          if r.incomes.present?
            if r.may_confirmar?
                r.confirmar
                r.save
            end
          end
        end
        if @person.tipo == "C"
          if params[:commit] == "Finalizar"
            format.html { redirect_to root_path , notice: 'Alquiler guardado correctamente.' }
          else  
            format.html { redirect_to clientes_path , notice: 'Cliente guardado correctamente.' }
          end
           
        else
          format.html { redirect_to people_path , notice: 'Dueño guardada correctamente.' }
        end
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
     
    respond_to do |format|
      if @person.update(person_params)
        
        if @person.tipo == "C"
          format.html { redirect_to clientes_path , notice: 'Cliente actualizado correctamente.' }
        else
          format.html { redirect_to people_path , notice: 'Dueño actualizado correctamente.' }
        end
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @tipo  = @person.tipo
    @person.destroy
    respond_to do |format|
      if @tipo == "C"
        format.html { redirect_to clientes_path , notice: 'Cliente borrado correctamente.' }
      else
        format.html { redirect_to people_path , notice: 'Dueño borrado correctamente.' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def person_params
      params.require(:person).permit(:documento, :nombres, :apellidos, :ciudad, :domicilio, :fecha_nac, :matricula_coche, :tipo,:segundo_ape, :segundo_nom, :pais_residencia, :pais_origen_doc, 
        :sexo,:nationality_id, :document_type_id, :cuenta_banco, phones_attributes: [ :id, :descripcion, :numero  ], emails_attributes: [ :id, :descripcion, :email, :_destroy ], rentals_attributes: [:id,:fechadesde, :fechahasta, :valordia, :medidoringreso, :medidorfin, :valorkw, :cant_acomp, :informado, :descripcion, :cottage_id,:fecha_entrega,:hora_entrega,:hora_entrada,:costo_total,:estado,:ganancia_dueno,:transporte,:id_dueno, incomes_attributes:[ :fecha, :tipo, :importe, :rental_id],
          companions_attributes: [ :id, :nombre, :seg_nombre, :apellido, :seg_apellido, :documento, :pais_origen_doc, :document_type_id, :sexo, :nationality_id, :fecha_nac, :pais_residencia, :rental_id ]
        ])
    end
end
