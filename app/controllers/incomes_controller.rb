class IncomesController < ApplicationController
  before_action :set_income, only: [:show, :edit, :update, :destroy]
  before_action :authenticate!
  # GET /incomes
  # GET /incomes.json
  def index
    @incomes = Income.all
  end

  # GET /incomes/1
  # GET /incomes/1.json
  def show
  end
  
  def modal_new
    @income = Income.new
    @income.rental = Rental.find(params[:idr])
    @commite = 'Aceptar'
    if params[:importe]
      @income.importe = params[:importe]
      @commite = 'Pagar'
    end
    render :json => { :form => render_to_string(:partial => 'modal', :locals => {:income => @income,:commite => @commite}) }   
  end

  
  def balance
  end
  
  def balance_fecha
    @incomes  = Income.where(fecha: params[:min]..params[:max])
    @expenses = Expense.where(fecha: params[:min]..params[:max])
    
    @fechadesde   = params[:min]
    @fechahasta   = params[:max]
    @fechaControl = params[:min]
    @total        = 0
    @totalegresos = 0
    @totalsaldo   = 0
    @totalgeneral = 0
    
    @cottages = Cottage.all
  end
  
  def balance_afecha
    @str1    = params[:fecha_d]
    @fecha_d = @str1.to_datetime.strftime("%Y-%m-%d")
    @str2    = params[:fecha_h]
    @fecha_h = @str2.to_datetime.strftime("%Y-%m-%d")

    @cottage  = Cottage.find(params[:cottage_id])
    @incomes  = Income.find_by_sql "SELECT * FROM incomes i, rentals r WHERE i.fecha >= '"+@fecha_d+"' and i.fecha <= '"+ @fecha_h+"' and i.rental_id= r.id and r.cottage_id = "+ params[:cottage_id] + " ORDER BY i.rental_id"
    @expenses = Expense.where(cottage_id: params[:cottage_id]).where(fecha:  @fecha_d..@fecha_h)
    
    @total_dias = 0
   
    
  end
  
  # GET /incomes/new
  def new
    @income = Income.new
    @rentals = Rental.where(:estado => :confirmada).or(Rental.where(:estado => :ingresada)).or(Rental.where(:estado => :encurso))
  end

  # GET /incomes/1/edit
  def edit
    @rentals = Rental.where(:estado => :confirmada).or(Rental.where(:estado => :ingresada)).or(Rental.where(:estado => :encurso))
  end

  # POST /incomes
  # POST /incomes.json
  def create
    @income = Income.new(income_params)
   
    respond_to do |format|
      if @income.save
        if @income.rental
          if params[:commit] == 'Pagar'
            @income.rental.pago_luz = 'S'
            @income.rental.save
          end
          if @income.rental.may_confirmar?
             @income.rental.confirmar
             @income.rental.save
          end
        end
        if params[:commit] == 'Aceptar'
          format.html { redirect_to root_path, notice: 'Ingreso guardado correctamente.' }
        else
          if params[:commit] == 'Pagar'
            format.html { redirect_to root_path, notice: 'Ingreso guardado correctamente.' }
          else
            format.html { redirect_to @income, notice: 'Ingreso guardado correctamente.' }
            format.json { render :show, status: :created, location: @income }
          end
        end
           
      else
        format.html { render :new }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /incomes/1
  # PATCH/PUT /incomes/1.json
  def update
    respond_to do |format|
      if @income.update(income_params)
        if @income.rental.may_confirmar?
           @income.rental.confirmar
           @income.rental.save
        end
        format.html { redirect_to @income, notice: 'Ingreso actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @income }
      else
        format.html { render :edit }
        format.json { render json: @income.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /incomes/1
  # DELETE /incomes/1.json
  def destroy
    @income.destroy
    respond_to do |format|
      format.html { redirect_to incomes_url, notice: 'Ingreso eliminado correctamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_income
      @income = Income.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def income_params
      params.require(:income).permit(:fecha, :tipo, :importe, :rental_id)
    end
end
