class IdentitiesController < ApplicationController
   before_action :set_identity, only: [:show, :edit, :update, :destroy]
   before_action :authenticate!
  def index
    @identities = Identity.all
   
  end
  def new
    @identity = Identity.new
  end

  def create
    @identity = Identity.new identity_params
    if @identity.save     
      redirect_to identities_path     
    else      
        respond_to do |format|
          format.html { render :new, alert: @identity.errors.full_messages }
          
        end
    end
      
  end
  
  # PATCH/PUT /identities/1
  # PATCH/PUT /identities/1.json
  def update
    respond_to do |format|
      if @identity.update(identity_params)
        format.html { redirect_to identity_path, notice: 'El Usuario se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @identity }
      else
        format.html { render :edit }
        format.json { render json: @identity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /identities/1
  # DELETE /identities/1.json
  def destroy
    if @identity.email != current_identity.email
      @identity.destroy
      respond_to do |format|
        format.html { redirect_to identities_url, notice: 'El Usuario se elimino correctamente.' }
        format.json { head :no_content }
      end
    else  
      respond_to do |format|
        format.html { redirect_to identities_url, alert: 'No se puede eliminar a si mismo.' }
        format.json { head :no_content }
      end
    end
  end

private
 # Use callbacks to share common setup or constraints between actions.
    def set_identity
      @identity = Identity.find(params[:id])
    end
  def identity_params
    params.require(:identity).permit :email, :password, :password_confirmation, :admin, :avatar
  end

end
