class SessionsController < ApplicationController
  before_action :authenticate! ,only: [:create, :destroy]
  def new
    if current_identity
      redirect_to root_path
    else
      @identity = Identity.new
    end
  end

  def create    
    warden.authenticate!(scope: :identity)
    redirect_to root_url, notice: t('.logged_in')    
  end

  def destroy
    warden.logout(:identity)
    redirect_to root_url, notice: t('.logged_out')
  end
end

