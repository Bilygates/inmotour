class RentalsController < ApplicationController
  before_action :set_rental, only: [:show, :edit, :update, :destroy, :informar]
  before_action :authenticate!
  # GET /rentals
  # GET /rentals.json

  def index
    @rentals = Rental.all
  end

  # GET /rentals/1
  # GET /rentals/1.json
  def show
    @rental.companions.build 
  end
  
  def informar
    @rental.informado = "S"
    @rental.save
  end

  
  
  def ingresopdf
    @rental = Rental.find(params[:id_rent])
    if @rental.confirmada? || @rental.ingresada?
      if @rental.may_cursar?
        @rental.cursar
        @rental.save
      end
    end
   render :layout => "empty"
  end
  
  
  def enviarCorreo
    ActionCorreoMailer.bienvenido_email('@gmail.com').deliver
  end
  
  def informado
    @rentals = Rental.all
    
    @rentals.each do |rental| 
      @companions = Companion.where(:rental_id => rental.id)
      @continuar = "S"
      if rental.informado != "S" 
        if rental.encurso? 
          if rental.person.documento != "" && rental.person.pais_origen_doc != '' && rental.person.document_type  && rental.person.sexo != "" && rental.person.nationality && rental.person.pais_residencia != "" && rental.fechadesde && rental.fechahasta && rental.person.fecha_nac 
            
            @companions.each do |companion| 
              if !companion.documento || !companion.nombre || !companion.apellido || !companion.pais_origen_doc || !companion.document_type || !companion.sexo || !companion.fecha_nac || !companion.pais_residencia || !companion.nationality 
                 @continuar = "N"
              end
            end
            
            if @continuar != "N"
              rental.informado = "S"
              rental.save(validate: false)
            end
          end
        end
      end
    end
  end

  
  def finalizar
      @rental = Rental.find(params[:id])
      if @rental.may_finalizar?
          @rental.finalizar
          if @rental.save(validate: false)
              render :json => @rental.to_json
          else  
              render :json => { :errors => @rental.errors.full_messages }
          end
      else
        render :json => { :errors => @rental.errors.full_messages }
      end
  end
  
  def cancelar
    
      @rental = Rental.find(params[:id])
      if !@rental.cancelada?
        if @rental.may_cancelar?
            @rental.cancelar
            if @rental.save(validate: false)
              render :json => @rental.to_json
            else  
              render :json => { :errors => @rental.errors.full_messages }
            end
        else
          render :json => { :errors => @rental.errors.full_messages }
        end
      else
        @rental.destroy
        render :json => @rental.to_json
      end
    
  end
  
  
  
  def controlarReserva
    
    @rentals = Rental.where(cottage_id: params[:cottage_id]).where(fechadesde: params[:fechadesde]..params[:fechahasta])
    @ok = 'S'
    if @rentals.any?
      @rentals.each do |r|
        if r.fecha_entrega > r.fechahasta
          @ok = 'N'
        end
      end
      if @ok == 'N'
        @error = 'Ya existe reserva para el periodo seleccionado.'
        render :json => { :errors => @error.to_json }
      end  
    end
    if @ok == 'S'
      @rentals = Rental.where(cottage_id: params[:cottage_id]).where(fechahasta:  params[:fechadesde]..params[:fechahasta]).where.not(fecha_entrega:  params[:fechadesde]..params[:fechahasta])
      if @rentals.any?
          @error = 'Ya existe reserva para el periodo seleccionado.'
         render :json => { :errors => @error.to_json }
      else  
        @rentals = Rental.where(cottage_id: params[:cottage_id]).where(fecha_entrega: params[:fechadesde]).where("hora_entrega > ?", params[:hora_entrada])   
        if @rentals.any?
          @error = 'La casa todavía esta ocupada a esa hora de entrada.'
          render :json => { :errors => @error.to_json }
        end
      end
    end
  end
  
    
  def modal_luz
    @rental = Rental.find(params[:idr])
    render :json => { :form => render_to_string(:partial => 'modalLuz', :locals => {:rental => @rental}) }   
  end
  
  # GET /rentals/new
  def new
    @rental = Rental.new
    @income = Income.new
    @rental.incomes.build 
    @rental.companions.build 
    @rental.estado = :ingresada
  end

  # GET /rentals/1/edit
  def edit
    @rental.incomes.build 
    @rental.companions.build 
    if @rental.incomes.present?
      if @rental.may_confirmar?
          @rental.confirmar
      end
    end
  end
  
  # POST /rentals
  # POST /rentals.json
  def create
    @rental = Rental.new(rental_params)

    respond_to do |format|
      if @rental.save
        if @rental.incomes.present?
          if @rental.may_confirmar?
              @rental.confirmar
              @rental.save
          end
        end
        format.html { redirect_to @rental, notice: 'Alquiler guardado correctamente.' }
        format.json { render :show, status: :created, location: @rental }
        
      else
        format.html { render :new }
        format.json { render json: @rental.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rentals/1
  # PATCH/PUT /rentals/1.json
  def update
    respond_to do |format|
      if @rental.update(rental_params)
        if @rental.incomes.present?
          if @rental.may_confirmar?
              @rental.confirmar
              @rental.save
          end
        end
        format.html { redirect_to @rental, notice: 'Alquiler actualizado correctamente.' }
        format.json { render :show, status: :ok, location: @rental }
      else
        format.html { render :edit }
        format.json { render json: @rental.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rentals/1
  # DELETE /rentals/1.json
  def destroy
    if @rental.cancelada?
      @rental.destroy
      respond_to do |format|
        if params[:commit] == 'Cancelar alquiler'
          format.html { redirect_to root_path, notice: 'Alquiler eliminado correctamente.' }
          format.json { head :no_content }
        else
          format.html { redirect_to rentals_url, notice: 'Alquiler eliminado correctamente.' }
          format.json { head :no_content }
        end
      end
    else
      respond_to do |format|
        if @rental.may_cancelar?
          @rental.cancelar   
          @rental.save
          if params[:commit] == 'Cancelar alquiler'
            format.html { redirect_to root_path, notice: 'Alquiler cancelado correctamente.' }
            format.json { head :no_content }
          else
            format.html { redirect_to rentals_url, notice: 'Alquiler cancelado correctamente.' }
            format.json { head :no_content }
          end
        else
           format.html { redirect_to root_path, notice: 'El Alquiler no puede ser cancelado.' }
           format.json { head :no_content }
        end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rental
      @rental = Rental.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rental_params
      params.require(:rental).permit(:fechadesde, :fechahasta, :valordia, :medidoringreso, :medidorfin, :valorkw, :cant_acomp, :descripcion, :person_id, :cottage_id,:fecha_entrega,:hora_entrega, :hora_entrada,:costo_total,:estado,:ganancia_dueno,:transporte, :informado, :total_luz, :pago_luz, incomes_attributes: [ :id, :fecha, :tipo, :importe  ], companions_attributes: [ :id, :nombre, :seg_nombre, :apellido, :seg_apellido, :documento, :pais_origen_doc, :document_type_id, :sexo, :nationality_id, :fecha_nac, :pais_residencia, :rental_id])
    end
end
