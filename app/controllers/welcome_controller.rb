class WelcomeController < ApplicationController
  def index
    if !current_identity
      redirect_to log_in_path  
    else
      @cottages = Cottage.all
      @rentals = Rental.all
      #index_wiz
    end
  end
  
  def index_wiz
    if !current_identity
      redirect_to log_in_path      
    else
      @person = Person.new
      @person.tipo ='C'
      @person.rentals.build do |r|
        r.incomes.build
        r.companions.build
      end
      @person.phones.build
      @person.emails.build
    
    end
  end
  
  
end
