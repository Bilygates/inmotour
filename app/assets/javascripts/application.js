//= require jquery
//= require jquery_ujs
//= require jquery-ui

// IMPORTANT: APP CONFIG
//= require app.config

// BOOTSTRAP JS
//= require bootstrap/bootstrap

// CUSTOM NOTIFICATION
//= require notification/SmartNotification

// JARVIS WIDGETS
//= require smartwidgets/jarvis.widget


// browser msie issue fix
//= require plugin/msie-fix/jquery.mb.browser

// FastClick: For mobile devices
//= require plugin/fastclick/fastclick

// MAIN APP JS FILE
//= require app

//***DATA TABLES
//= require plugin/datatable-responsive/datatables.responsive
//= require plugin/datatables/jquery.dataTables
//= require plugin/datatables/dataTables.bootstrap
//= require plugin/datatables/dataTables.buttons.min
//= require plugin/datatables/jszip.min
//= require plugin/datatables/pdfmake.min
//= require plugin/datatables/vfs_fonts
//= require plugin/datatables/buttons.html5.min


//= require plugin/bootstrapvalidator/bootstrapValidator

//= require plugin/bootstrap-wizard/jquery.bootstrap.wizard

//= require plugin/jquery-validate/jquery.validate

//***Full Calendar
//= require plugin/fullcalendar/moment.min
//= require plugin/fullcalendar/fullcalendar.min
//= require plugin/fullcalendar/scheduler.min
// require plugin/fullcalendar/locale-all

//= require jquery_nested_form


//= require sweetalert2
// require rails_confirm_override

// require moment




