Rails.application.routes.draw do

  resources :companions
  resources :providers
  resources :expenses
  resources :incomes do 
    collection do 
      get :modal_new
    end
  end
  resources :rentals do
    collection do 
      post :finalizar
      post :cancelar
      post :informado
      post :enviarCorreo
      post :controlarReserva
      get :modal_luz
    end
  end
  
  resources :phones
  resources :emails
  resources :people
  resources :document_types
  resources :categories
  resources :nationalities
  resources :cottages
  resource :sessions, only: [:create, :new]
  resources :identities

  
  delete 'log_out' => 'sessions#destroy', as: :log_out
  get 'log_in' => 'sessions#new', as: :log_in
  get 'sign_up' => 'identities#new', as: :sign_up
  
  get 'welcome/index'
  get 'welcome/index_wiz'
  get 'welcome/calendarfull'
  
  get 'clientes' => 'people#index_cliente'
  get 'duenoscobro/:id_dueno', to: 'people#duenoscobro', as:'duenoscobro'
  get 'reporteduenos', to: 'people#reporteduenos', as:'reporteduenos'
  get 'clientes_rentas', to: 'people#clientes_rentas', as:'clientes_rentas'
  get 'clientes_rentas_faltantes', to: 'people#clientes_rentas_faltantes', as:'clientes_rentas_faltantes'
  get 'agendacuentas', to: 'people#agendacuentas', as:'agendacuentas'
  
  get 'reporte_ganancias', to: 'people#reporte_ganancias'
  get 'reporte_ganancias_detalle', to: 'people#reporte_ganancias_detalle', as: 'reporte_ganancias_detalle'

  get 'reporteCasa/:id_rep', to: 'cottages#reporte', as: 'rep'
  get 'calendariocasa', to: 'cottages#calendariocasa'
  get 'balance_fecha', to: 'incomes#balance_fecha', as: 'balance_fecha'
  get 'balance_afecha/:cottage_id/:fecha_d/:fecha_h', to: 'incomes#balance_afecha', as: 'balance_afecha'
  get 'balance', to: 'incomes#balance', as: 'balance'

  get 'ingresopdf/:id_rent', to: 'rentals#ingresopdf', as:'ingresopdf'
 
  root 'welcome#index'


  # CK editor
  mount Ckeditor::Engine => '/ckeditor'
end
